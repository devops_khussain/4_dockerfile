FROM openjdk:17-alpine
RUN mkdir /app
COPY . /app
WORKDIR /app
RUN rm -rf /var/cache/apk/*
EXPOSE 8080
CMD ["java", "-jar", "target/hallo-0.0.1-SNAPSHOT.jar"]
